// SPDX-License-Identifier: CC0-1.0
// Copyright: 2022 YOSHIOKA Takuma <nop_thread@nops.red>
#define RESET_TYPE EfiResetWarm
#define RESET_MESSAGE L"Warm reset..."
#include "base.c"
