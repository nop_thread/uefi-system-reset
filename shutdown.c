// SPDX-License-Identifier: CC0-1.0
// Copyright: 2022 YOSHIOKA Takuma <nop_thread@nops.red>
#define RESET_TYPE EfiResetShutdown
#define RESET_MESSAGE L"Powering off..."
#include "base.c"
