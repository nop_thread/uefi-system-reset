// SPDX-License-Identifier: CC0-1.0
// Copyright: 2022 YOSHIOKA Takuma <nop_thread@nops.red>
#include <efi.h>
#include <efilib.h>

#ifndef RESET_TYPE
// * EfiResetCold: Cold reset.
// * EfiResetWarm: Warm reset.
// * EfiResetShutdown: Shutdown, or cold reset if not supported by the platform.
// * EfiResetPlatformSpecific: Platform-specific reset depending on `ResetData` parameter.
#define RESET_TYPE EfiResetShutdown
#endif

EFI_STATUS EFIAPI efi_main(EFI_HANDLE image, EFI_SYSTEM_TABLE *systab) {
	InitializeLib(image, systab);

	// Print message.
#ifdef RESET_MESSAGE
	uefi_call_wrapper(systab->ConOut->OutputString, 2, systab->ConOut, RESET_MESSAGE);
#endif

	// Reset.
	return uefi_call_wrapper(
		systab->RuntimeServices->ResetSystem,
		4,
		RESET_TYPE,
		// EFI_SUCCESS: This is normal intentional reset.
		EFI_SUCCESS,
		// No additional data.
		0,
		NULL);
}
