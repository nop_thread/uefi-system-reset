# SPDX-License-Identifier: CC0-1.0
# Copyright: 2022 YOSHIOKA Takuma <nop_thread@nops.red>
ifeq ($(ARCH),)
	ARCH := $(shell uname -m)
endif
EFI_INC = /usr/include/efi
EFI_LIB = /usr/lib

OBJCOPY = objcopy

EFI_CRTOBJS = $(EFI_LIB)/crt0-efi-$(ARCH).o
EFI_LDSCRIPT = $(EFI_LIB)/elf_$(ARCH)_efi.lds
INCLUDES = -I$(EFI_INC) -I$(EFI_INC)/$(ARCH) -I$(EFI_INC)/protocol

# References:
# * <https://wiki.osdev.org/GNU-EFI>


# -fpic: UEFI PE executables must be position independent.
# -ffreestanding: No libc.
# -fno-stack-protector -fno-stack-check -mno-red-zone: Don't do fancy stuff using call stack.
# -fshort-wchar: UEFI APIs use UTF-16 string, so use 2 bytes character as wchar_t.
CFLAGS = $(EFIINC) -std=c18 -Wall \
		 -fpic -fno-stack-protector -fno-stack-check -mno-red-zone \
		 -fshort-wchar \
		 $(INCLUDES)
ifeq ($(ARCH),x86_64)
	CFLAGS += -DEFI_FUNCTION_WRAPPER
	CFLAGS += -m64
else
	CFLAGS += -m32
endif
LDFLAGS = -nostdlib -znocombreloc -T $(EFI_LDSCRIPT) -shared -Bsymbolic -L $(EFI_LIB) $(EFI_CRTOBJS)
OBJCOPYFLAGS = -j .text -j .sdata -j .data -j .dynamic -j .dynsym \
			   -j .rel -j .rela -j .rel.* -j .rela.* -j .reloc \
			   --target=efi-app-$(ARCH) --subsystem=10

OBJS = shutdown.o cold-reset.o warm-reset.o
OBJS_SHARED = shutdown.so cold-reset.so warm-reset.so
TARGETS = shutdown.efi cold-reset.efi warm-reset.efi

.PHONY: all clean

all: $(TARGETS)

clean:
	$(RM) -f $(TARGETS) $(OBJS_SHARED) $(OBJS)

%.efi: %.so
	$(OBJCOPY) $(OBJCOPYFLAGS) $^ $@

%.so: %.o
	$(LD) $(LDFLAGS) $^ -o $@ -lefi -lgnuefi
