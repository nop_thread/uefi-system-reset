// SPDX-License-Identifier: CC0-1.0
// Copyright: 2022 YOSHIOKA Takuma <nop_thread@nops.red>
#define RESET_TYPE EfiResetCold
#define RESET_MESSAGE L"Cold reset..."
#include "base.c"
